
<input type="radio" name="clients" id="client-tab-1" class="client-tab-radio client-tab-radio-1 none" checked>
<label class="client-tab-label client-tab-label-1" for="client-tab-1" onclick><span class="none">Client Tab</span></label>
<div class="client-tab client-tab-1">
    <ul class="clients-list clear">
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/oracle.png" alt="Oracle"></a>
        </li>
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/sony.png" alt="Sony"></a>
        </li>
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/sandisk.png" alt="SanDisk"></a>
        </li>
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/mcafee.png" alt="McAfee"></a>
        </li>
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/netflix.png" alt="Netflix"></a>
        </li>
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/sap.png" alt="SAP"></a>
        </li>
    </ul>
</div>
<input type="radio" name="clients" id="client-tab-2" class="client-tab-radio client-tab-radio-2 none">
<label class="client-tab-label client-tab-label-2" for="client-tab-2" onclick><span class="none">Client Tab</span></label>
<div class="client-tab client-tab-2">
    <ul class="clients-list clear">
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/sony.png" alt="Sony"></a>
        </li>
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/oracle.png" alt="Oracle"></a>
        </li>
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/sandisk.png" alt="SanDisk"></a>
        </li>
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/mcafee.png" alt="McAfee"></a>
        </li>
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/sap.png" alt="SAP"></a>
        </li>
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/netflix.png" alt="Netflix"></a>
        </li>
    </ul>
</div>
<input type="radio" name="clients" id="client-tab-3" class="client-tab-radio client-tab-radio-3 none">
<label class="client-tab-label client-tab-label-3" for="client-tab-3" onclick><span class="none">Client Tab</span></label>
<div class="client-tab client-tab-3">
    <ul class="clients-list clear">
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/netflix.png" alt="Netflix"></a>
        </li>
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/oracle.png" alt="Oracle"></a>
        </li>
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/sony.png" alt="Sony"></a>
        </li>
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/mcafee.png" alt="McAfee"></a>
        </li>
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/sandisk.png" alt="SanDisk"></a>
        </li>
        <li class="client-item col-6 col-4-l">
            <a href="#"><img class="block img-full" src="<?php echo base_url() ?>resources/img/sap.png" alt="SAP"></a>
        </li>
    </ul>
</div>