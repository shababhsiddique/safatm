<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $this->content->config("site_title");?></title>

        <meta name="description" content="">
        <meta name="theme-color" content="#CCCCCC">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="<?php echo $this->content->config("site_logo");?>">
        <link rel="apple-touch-icon" sizes="57x57" href="img/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">


        <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js" ></script>
        
        
        <link rel="stylesheet" href="<?php echo base_url()?>resources/css/styles.css">
    
        <?php $this->load->view("admin/components/wysiwyg/nicinit"); ?>
    </head>    
    <body>
        <?php $this->load->view("admin/components/wysiwyg/nicpanel"); ?>

        <div class="banner">
            <div class="header">
                <div class="header-inner container clear">
                    <a class="logo" href="#"><span class="sr">Skokov</span></a>
                    <input type="checkbox" id="navigation-toggle-checkbox" name="navigation-toggle-checkbox" class="navigation-toggle-checkbox none">
                    <label for="navigation-toggle-checkbox" class="navigation-toggle-label" onclick>
                        <span class="navigation-toggle-label-inner">
                            <span class="sr">Navigation</span>
                        </span>
                    </label>
                    <div class="navigation">
                        <ul class="navigation-menu">
                            <li class="navigation-item"><a href="#home">Home</a></li>
                            <li class="navigation-item"><a href="#services">Services</a></li>
                            <li class="navigation-item"><a href="#about">About Us</a></li>
                            <li class="navigation-item"><a href="#news">News</a></li>
                            <li class="navigation-item"><a href="#contact">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="banner-inner">
                    <?php echo $this->content->block("banner1") ?>
                    <div class="banner-buttons">
                        <a href="#services" class="button button-primary">Get started</a>
                        <a href="#about" class="button button-secondary">Learn more</a>
                    </div>
                </div>
            </div>
        </div>

        <div id="services" class="animate-block content-block services-block">
            <div class="services-block-inner container">
                <div class="content-block-inner text-center">
                    <h2 class="uppercase pad-bottom-20">Our Services</h2>
                    <div class="col-10-l no-float center">
                        <?php echo $this->content->block("ourservices") ?>
                    </div>
                </div>
                <?php echo $this->content->block("servicelist") ?>
            </div>
        </div>

        <div class="animate-block feature-block contact-feature-block">
            <div class="feature-block-inner container">
                <div class="text-center">
                    <h2 class="feature-block-heading"><?php echo $this->content->block('contacthead')?></h2>
                    <a class="button button-primary" href="#contact">Contact Us</a>
                </div>
            </div>
        </div>

        <div id="about" class="content-block about-block">
            <div class="about-block-inner container">
                <div class="animate-block content-block-inner text-center">
                    <h2 class="uppercase pad-bottom-20">About Us</h2>
                    <div class="col-10-l no-float center">
                        <?php echo $this->content->block("aboutushead");?>
                    </div>
                </div>
                <div class="animate-block about-content clear row-m">
                    <div class="what-we-do-block col-6-m pad-top-20">
                        <?php echo $this->content->block("whatwedo")?>
                    </div>
                    <div class="our-clients-block col-6-m clear pad-top-20">
                        <h3 class="heading fs-3">Our Clients</h3>
                        <?php //$this->load->view("app/theme/ourclients");?>
                    </div>
                </div>

                <?php //$this->load->view("app/theme/team")?>

                
            </div>
        </div>

        <div id="video-feature-block" class="feature-block video-feature-block">
            <div class="video-feature-inner container">
                <div class="animate-block content-block-inner text-center">
                    <a id="video" class="button button-play" href="#feature-video"><span class="sr">Play</span></a>
                    <?php echo $this->content->block("exitbanner")?>
                </div>
                <div class="modal" id="feature-video">
                    <a href="#video" class="modal-close"><span class="sr">Close</span></a>
                    <div class="modal-inner">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div id="news" class="animate-block content-block news-block">
            <div class="news-block-inner container">
                <div class="content-block-inner text-center">
                    <h2 class="uppercase pad-bottom-20">News</h2>
                    <div class="col-10-l no-float center">
                        <?php echo $this->content->block("newshead")?>
                    </div>
                </div>
                <div class="animate-block news-list clear">
                    <div class="news-list-left col-8-l no-padding-l">
                        <div class="news-item clear">
                            <a href="#" class="news-item-image col-7-l no-padding-l">
                                <img class="block img-fluid" src="<?php echo base_url()?>resources/img/news-item-01.jpg" alt="News Item 01">
                            </a>
                            <div class="news-item-content col-5-l no-padding-l">
                                <div class="news-item-content-inner">
                                    <h3 class="news-item-title uppercase"><a href="#">Why Financial Institutions need a new IT model</a></h3>
                                    <p>
                                        Why old IT models are unlikely to deliver the competitive edge that banks, insurers and wealth management firms need from technology.
                                    </p>
                                    <em class="news-item-publish-date">
                                        Posted: 08.07.2015
                                    </em>
                                </div>
                            </div>
                        </div>
                        <div class="news-item news-item-secondary clear">
                            <a href="#" class="news-item-image col-7-l no-padding-l">
                                <img class="block img-fluid" src="<?php echo base_url()?>resources/img/news-item-02.jpg" alt="News Item 02">
                            </a>
                            <div class="news-item-content col-5-l no-padding-l">
                                <div class="news-item-content-inner">
                                    <h3 class="news-item-title uppercase"><a href="#">Reimagining IT for an Omnichannel world</a></h3>
                                    <p>
                                        The technology that powers retail is evolving rapidly. Retailers and their IT groups will have to rise to stay ahead.
                                    </p>
                                    <em class="news-item-publish-date">
                                        Posted: 08.07.2015
                                    </em>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="news-list-right col-4-l no-padding-l">
                        <div class="news-item news-item-third clear">
                            <a href="#" class="news-item-image col-7-l no-padding-l">
                                <img class="block img-fluid" src="<?php echo base_url()?>resources/img/news-item-03.jpg" alt="News Item 03">
                            </a>
                            <div class="news-item-content col-5-l no-padding-l">
                                <div class="news-item-content-inner">
                                    <h3 class="news-item-title uppercase"><a href="#">Making the most of generation assets in turbulent times</a></h3>
                                    <p>
                                        A thorough review of a power generation portfolio informs decisions about how to reduce costs, raise revenues and make strategic plans.
                                    </p>
                                    <em class="news-item-publish-date">
                                        Posted: 08.07.2015
                                    </em>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="contact" class="animate-block content-block contact-block">
            <div class="contact-block-inner container">
                <div class="clear">
                    <div class="contact-block-content col-6-m">
                        <?php echo $this->content->block("prefooter")?>
                    </div>
                    <div class="contact-block-form col-6-m">
                        <h2 class="fs-2 heading">Contact</h2>
                        <p>
                            Please contact us using contact form below.
                        </p>
                        <form class="contact-form">
                            <fieldset class="contact-form-wrap">
                                <legend class="sr">Contact Us</legend>
                                <div class="pad-top-5 pad-bottom-5">
                                    <label class="sr" for="name">Name</label>
                                    <input name="name" class="field" id="name" type="text" placeholder="Name">
                                </div>
                                <div class="pad-top-5 pad-bottom-5">
                                    <label class="sr" for="email">Email</label>
                                    <input name="email" class="field" id="email" type="email" placeholder="Email">
                                </div>
                                <div class="pad-top-5 pad-bottom-5">
                                    <label class="sr" for="subject">Date</label>
                                    <input name="subject" class="field" id="subject" type="text" placeholder="Subject">
                                </div>
                                <div class="pad-top-5 pad-bottom-5">
                                    <label for="message" class="sr">Message</label>
                                    <textarea placeholder="Message" class="field" name="message" id="message" cols="30" rows="7"></textarea>
                                </div>
                                <div class="text-right">
                                    <input type="submit" class="button button-primary contact-submit" value="Send">
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="footer-inner container">
                <div class="clear">
                    <div class="footer-column col-8-m">
                        <p>
                            &copy; Copyright 2016 &mdash; All Rights Reserved
                        </p>
                    </div>
                    <div class="footer-column col-4-m">
                        <ul class="footer-social-list icon-list-inline">
                            <li class="navigation-item-social"><a class="social-icon social-linkedin" href="#"><span class="sr">LinkedIn</span></a></li>
                            <li class="navigation-item-social"><a class="social-icon social-facebook" href="#"><span class="sr">Facebook</span></a></li>
                            <li class="navigation-item-social"><a class="social-icon social-twitter" href="#"><span class="sr">Twitter</span></a></li>
                            <li class="navigation-item-social"><a class="social-icon social-youtube" href="#"><span class="sr">YouTube</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <script src="<?php echo base_url() ?>resources/js/vendor/wow.js"></script>
        <script src="<?php echo base_url() ?>resources/js/default.js"></script>
        <script src="<?php echo base_url() ?>resources/js/script.js"></script>

    </body>
</html>