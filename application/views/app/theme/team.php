<div class="animate-block our-team-block pad-top-20">
    <h3 class="heading fs-3">Our Team</h3>
    <ul id="team" class="team-list clear">
        <li class="team-item col-6 col-4-m no-padding-m col-3-l no-padding-l col-2-xl no-padding-xl">
            <a id="team-1" href="#team-1-profile"><img class="block img-full" src="<?php echo base_url() ?>resources/img/team-01.jpg" alt="Team Member 1">
                <span class="team-item-content">
                    <strong class="team-item-name">Clark Kent</strong>
                    <strong class="team-item-title">Bad Debt Manager</strong>
                    <span class="team-item-position">Team Leader</span>
                </span>
            </a>
        </li>
        <li class="team-item col-6 col-4-m no-padding-m col-3-l no-padding-l col-2-xl no-padding-xl">
            <a id="team-2" href="#team-2-profile"><img class="block img-full" src="<?php echo base_url() ?>resources/img/team-02.jpg" alt="Team Member 2">
                <span class="team-item-content">
                    <strong class="team-item-name">Clark Kent</strong>
                    <strong class="team-item-title">Bad Debt Manager</strong>
                    <span class="team-item-position">Team Leader</span>
                </span>
            </a>
        </li>
        <li class="team-item col-6 col-4-m no-padding-m col-3-l no-padding-l col-2-xl no-padding-xl">
            <a id="team-3" href="#team-3-profile"><img class="block img-full" src="<?php echo base_url() ?>resources/img/team-03.jpg" alt="Team Member 3">
                <span class="team-item-content">
                    <strong class="team-item-name">Clark Kent</strong>
                    <strong class="team-item-title">Bad Debt Manager</strong>
                    <span class="team-item-position">Team Leader</span>
                </span>
            </a>
        </li>
        <li class="team-item col-6 col-4-m no-padding-m col-3-l no-padding-l col-2-xl no-padding-xl">
            <a id="team-4" href="#team-4-profile"><img class="block img-full" src="<?php echo base_url() ?>resources/img/team-04.jpg" alt="Team Member 4">
                <span class="team-item-content">
                    <strong class="team-item-name">Clark Kent</strong>
                    <strong class="team-item-title">Bad Debt Manager</strong>
                    <span class="team-item-position">Team Leader</span>
                </span>
            </a>
        </li>
        <li class="team-item col-6 col-4-m no-padding-m col-3-l no-padding-l col-2-xl no-padding-xl">
            <a id="team-5" href="#team-5-profile"><img class="block img-full" src="<?php echo base_url() ?>resources/img/team-05.jpg" alt="Team Member 5">
                <span class="team-item-content">
                    <strong class="team-item-name">Clark Kent</strong>
                    <strong class="team-item-title">Bad Debt Manager</strong>
                    <span class="team-item-position">Team Leader</span>
                </span>
            </a>
        </li>
        <li class="team-item col-6 col-4-m no-padding-m col-3-l no-padding-l col-2-xl no-padding-xl">
            <a id="team-6" href="#team-6-profile"><img class="block img-full" src="<?php echo base_url() ?>resources/img/team-06.jpg" alt="Team Member 6">
                <span class="team-item-content">
                    <strong class="team-item-name">Clark Kent</strong>
                    <strong class="team-item-title">Bad Debt Manager</strong>
                    <span class="team-item-position">Team Leader</span>
                </span>
            </a>
        </li>
        <li class="team-item col-6 col-4-m no-padding-m col-3-l no-padding-l col-2-xl no-padding-xl">
            <a id="team-7" href="#team-7-profile"><img class="block img-full" src="<?php echo base_url() ?>resources/img/team-07.jpg" alt="Team Member 7">
                <span class="team-item-content">
                    <strong class="team-item-name">Clark Kent</strong>
                    <strong class="team-item-title">Bad Debt Manager</strong>
                    <span class="team-item-position">Team Leader</span>
                </span>
            </a>
        </li>
        <li class="team-item col-6 col-4-m no-padding-m col-3-l no-padding-l col-2-xl no-padding-xl">
            <a id="team-8" href="#team-8-profile"><img class="block img-full" src="<?php echo base_url() ?>resources/img/team-08.jpg" alt="Team Member 8">
                <span class="team-item-content">
                    <strong class="team-item-name">Clark Kent</strong>
                    <strong class="team-item-title">Bad Debt Manager</strong>
                    <span class="team-item-position">Team Leader</span>
                </span>
            </a>
        </li>
        <li class="team-item col-6 col-4-m no-padding-m col-3-l no-padding-l col-2-xl no-padding-xl">
            <a id="team-9" href="#team-9-profile"><img class="block img-full" src="<?php echo base_url() ?>resources/img/team-09.jpg" alt="Team Member 9">
                <span class="team-item-content">
                    <strong class="team-item-name">Clark Kent</strong>
                    <strong class="team-item-title">Bad Debt Manager</strong>
                    <span class="team-item-position">Team Leader</span>
                </span>
            </a>
        </li>
        <li class="team-item col-6 col-4-m no-padding-m col-3-l no-padding-l col-2-xl no-padding-xl">
            <a id="team-10" href="#team-10-profile"><img class="block img-full" src="<?php echo base_url() ?>resources/img/team-10.jpg" alt="Team Member 10">
                <span class="team-item-content">
                    <strong class="team-item-name">Clark Kent</strong>
                    <strong class="team-item-title">Bad Debt Manager</strong>
                    <span class="team-item-position">Team Leader</span>
                </span>
            </a>
        </li>
        <li class="team-item col-6 col-4-m no-padding-m col-3-l no-padding-l col-2-xl no-padding-xl">
            <a id="team-11" href="#team-11-profile"><img class="block img-full" src="<?php echo base_url() ?>resources/img/team-11.jpg" alt="Team Member 11">
                <span class="team-item-content">
                    <strong class="team-item-name">Clark Kent</strong>
                    <strong class="team-item-title">Bad Debt Manager</strong>
                    <span class="team-item-position">Team Leader</span>
                </span>
            </a>
        </li>
        <li class="team-item col-6 col-4-m no-padding-m col-3-l no-padding-l col-2-xl no-padding-xl">
            <a id="team-12" href="#team-12-profile"><img class="block img-full" src="<?php echo base_url() ?>resources/img/team-12.jpg" alt="Team Member 12">
                <span class="team-item-content">
                    <strong class="team-item-name">Clark Kent</strong>
                    <strong class="team-item-title">Bad Debt Manager</strong>
                    <span class="team-item-position">Team Leader</span>
                </span>
            </a>
        </li>
    </ul>
</div>

<div class="team-profiles-block">
                    <div class="modal" id="team-1-profile">
                        <a href="#team-1" class="modal-close"><span class="sr">Close</span></a>
                        <div class="modal-inner">
                            <h2 class="heading fs-3 team-profile-heading">Clark Kent</h2>
                            <strong class="block">Bad Debt Manager - Team Leader</strong>
                            <div class="clear team-profile-content">
                                <img class="team-profile-image block img-fluid" src="<?php echo base_url()?>resources/img/team-01.jpg" alt="Team Member 1">
                                <div class="team-profile-content-block">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal" id="team-2-profile">
                        <a href="#team-2" class="modal-close"><span class="sr">Close</span></a>
                        <div class="modal-inner">
                            <h2 class="heading fs-3 team-profile-heading">Clark Kent</h2>
                            <strong class="block">Bad Debt Manager - Team Leader</strong>
                            <div class="clear team-profile-content">
                                <img class="team-profile-image block img-fluid" src="<?php echo base_url()?>resources/img/team-02.jpg" alt="Team Member 2">
                                <div class="team-profile-content-block">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal" id="team-3-profile">
                        <a href="#team-3" class="modal-close"><span class="sr">Close</span></a>
                        <div class="modal-inner">
                            <h2 class="heading fs-3 team-profile-heading">Clark Kent</h2>
                            <strong class="block">Bad Debt Manager - Team Leader</strong>
                            <div class="clear team-profile-content">
                                <img class="team-profile-image block img-fluid" src="<?php echo base_url()?>resources/img/team-03.jpg" alt="Team Member 3">
                                <div class="team-profile-content-block">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal" id="team-4-profile">
                        <a href="#team-4" class="modal-close"><span class="sr">Close</span></a>
                        <div class="modal-inner">
                            <h2 class="heading fs-3 team-profile-heading">Clark Kent</h2>
                            <strong class="block">Bad Debt Manager - Team Leader</strong>
                            <div class="clear team-profile-content">
                                <img class="team-profile-image block img-fluid" src="<?php echo base_url()?>resources/img/team-04.jpg" alt="Team Member 4">
                                <div class="team-profile-content-block">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal" id="team-5-profile">
                        <a href="#team-5" class="modal-close"><span class="sr">Close</span></a>
                        <div class="modal-inner">
                            <h2 class="heading fs-3 team-profile-heading">Clark Kent</h2>
                            <strong class="block">Bad Debt Manager - Team Leader</strong>
                            <div class="clear team-profile-content">
                                <img class="team-profile-image block img-fluid" src="<?php echo base_url()?>resources/img/team-05.jpg" alt="Team Member 5">
                                <div class="team-profile-content-block">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal" id="team-6-profile">
                        <a href="#team-6" class="modal-close"><span class="sr">Close</span></a>
                        <div class="modal-inner">
                            <h2 class="heading fs-3 team-profile-heading">Clark Kent</h2>
                            <strong class="block">Bad Debt Manager - Team Leader</strong>
                            <div class="clear team-profile-content">
                                <img class="team-profile-image block img-fluid" src="<?php echo base_url()?>resources/img/team-06.jpg" alt="Team Member 6">
                                <div class="team-profile-content-block">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal" id="team-7-profile">
                        <a href="#team-7" class="modal-close"><span class="sr">Close</span></a>
                        <div class="modal-inner">
                            <h2 class="heading fs-3 team-profile-heading">Clark Kent</h2>
                            <strong class="block">Bad Debt Manager - Team Leader</strong>
                            <div class="clear team-profile-content">
                                <img class="team-profile-image block img-fluid" src="<?php echo base_url()?>resources/img/team-07.jpg" alt="Team Member 7">
                                <div class="team-profile-content-block">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal" id="team-8-profile">
                        <a href="#team-8" class="modal-close"><span class="sr">Close</span></a>
                        <div class="modal-inner">
                            <h2 class="heading fs-3 team-profile-heading">Clark Kent</h2>
                            <strong class="block">Bad Debt Manager - Team Leader</strong>
                            <div class="clear team-profile-content">
                                <img class="team-profile-image block img-fluid" src="<?php echo base_url()?>resources/img/team-08.jpg" alt="Team Member 8">
                                <div class="team-profile-content-block">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal" id="team-9-profile">
                        <a href="#team-9" class="modal-close"><span class="sr">Close</span></a>
                        <div class="modal-inner">
                            <h2 class="heading fs-3 team-profile-heading">Clark Kent</h2>
                            <strong class="block">Bad Debt Manager - Team Leader</strong>
                            <div class="clear team-profile-content">
                                <img class="team-profile-image block img-fluid" src="<?php echo base_url()?>resources/img/team-09.jpg" alt="Team Member 9">
                                <div class="team-profile-content-block">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal" id="team-10-profile">
                        <a href="#team-10" class="modal-close"><span class="sr">Close</span></a>
                        <div class="modal-inner">
                            <h2 class="heading fs-3 team-profile-heading">Clark Kent</h2>
                            <strong class="block">Bad Debt Manager - Team Leader</strong>
                            <div class="clear team-profile-content">
                                <img class="team-profile-image block img-fluid" src="<?php echo base_url()?>resources/img/team-10.jpg" alt="Team Member 10">
                                <div class="team-profile-content-block">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal" id="team-11-profile">
                        <a href="#team-11" class="modal-close"><span class="sr">Close</span></a>
                        <div class="modal-inner">
                            <h2 class="heading fs-3 team-profile-heading">Clark Kent</h2>
                            <strong class="block">Bad Debt Manager - Team Leader</strong>
                            <div class="clear team-profile-content">
                                <img class="team-profile-image block img-fluid" src="<?php echo base_url()?>resources/img/team-11.jpg" alt="Team Member 11">
                                <div class="team-profile-content-block">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal" id="team-12-profile">
                        <a href="#team-12" class="modal-close"><span class="sr">Close</span></a>
                        <div class="modal-inner">
                            <h2 class="heading fs-3 team-profile-heading">Clark Kent</h2>
                            <strong class="block">Bad Debt Manager - Team Leader</strong>
                            <div class="clear team-profile-content">
                                <img class="team-profile-image block img-fluid" src="<?php echo base_url()?>resources/img/team-12.jpg" alt="Team Member 12">
                                <div class="team-profile-content-block">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>